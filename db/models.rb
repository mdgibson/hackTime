# Players of the game which can then be associated with characters
# controlled by them
class Player < ActiveRecord::Base
  has_many :characters, dependent: :destroy
  validates_presence_of :name
end

# Characters are associated with weapons and masteries.  They can attack_mod
# with many different weapons
class Character < ActiveRecord::Base
  belongs_to :player
  has_many :masteries, dependent: :destroy
  has_many :weapons, through: :masteries
  validates_presence_of :name

  def next_attack_in(current_time = 0)
    if self.engaged or self.cdg_until > current_time
      self.initiative = self.initiative - (current_time - self.last_time)
    else
      self.initiative = self.initiative - 2 * (current_time - self.last_time)
    end

    self.last_time = current_time
    self.initiative
  end

  def cdg(current_time = 0)
    self.cdg_until = current_time + self.cdg_speed
    self.initiative += self.cdg_speed
  end

  def attack(mastery)
    swing_time = mastery.weapon.base_speed + mastery.speed_mod
    self.initiative = self.initiative <= 0 ? swing_time : self.initiative + swing_time
  end
end

# Weapons can be used by many characters and have base statistics which can
# be modified by Characters' abilities.
class Weapon < ActiveRecord::Base
  has_many :masteries
  has_many :characters, through: :masteries
  validates_presence_of :name
  validates_presence_of :base_speed
end

# Masteries are associated with both a character and a weapon.  They represent
# the training completed by a character and allow for improvement of things
# likelihood to hit, base speed, damage, attack, etc.
class Mastery < ActiveRecord::Base
  belongs_to :character
  belongs_to :weapon
  validates_presence_of :speed_mod
end
