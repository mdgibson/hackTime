# Add booleans to character to determine if it should be visible on player
# pages and on characters index page (gm only characters)
class AddVisibilityToCharacters < ActiveRecord::Migration
  def change
    change_table :characters do |t|
      t.boolean :is_visible
      t.boolean :gm_only
    end
  end
end
