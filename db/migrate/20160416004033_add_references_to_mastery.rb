# Add reference rows to masteries table
class AddReferencesToMastery < ActiveRecord::Migration
  def change
    change_table :masteries do |t|
      t.references :weapon
      t.references :character
    end
  end
end
