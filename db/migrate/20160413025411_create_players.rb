# create the players table
class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :name
      t.datetime :created_at
    end
  end
end
