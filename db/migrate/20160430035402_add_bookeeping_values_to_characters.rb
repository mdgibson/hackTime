class AddBookeepingValuesToCharacters < ActiveRecord::Migration
  def change
    change_table :characters do |t|
      t.integer :cdg_until, default: 0
      t.integer :last_time, default: 0
    end
  end
end
