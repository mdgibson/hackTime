# Add references rows to weapons table
class AddReferencesToWeapons < ActiveRecord::Migration
  def change
    change_table :weapons do |t|
      t.references :character
      t.references :mastery
    end
  end
end
