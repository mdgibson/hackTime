# Add references row to players table
class AddReferencesToPlayers < ActiveRecord::Migration
  def change
    change_table :players do |t|
      t.references :character
    end
  end
end
