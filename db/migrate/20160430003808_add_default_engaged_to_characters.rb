# Characters should not be engaged in combat by default
class AddDefaultEngagedToCharacters < ActiveRecord::Migration
  def change
    change_column_default(:characters, :engaged, false)
  end
end
