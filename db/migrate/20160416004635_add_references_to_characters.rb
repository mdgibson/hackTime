# add references rows to characters table
class AddReferencesToCharacters < ActiveRecord::Migration
  def change
    change_table :characters do |t|
      t.references :weapon
      t.references :mastery
      t.references :player
    end
  end
end
