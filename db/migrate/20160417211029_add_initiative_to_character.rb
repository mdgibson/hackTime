# add timing functionality rows to characters table
class AddInitiativeToCharacter < ActiveRecord::Migration
  def change
    change_table :characters do |t|
      t.boolean :engaged
      t.integer :initiative
    end
  end
end
