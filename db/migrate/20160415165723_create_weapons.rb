# create the weapons table
class CreateWeapons < ActiveRecord::Migration
  def change
    create_table :weapons do |t|
      t.string :name
      t.string :base_damage
      t.string :base_speed
      t.float :base_reach
    end
  end
end
