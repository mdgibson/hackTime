# Create the characters table
class CreateCharacters < ActiveRecord::Migration
  def change
    create_table :characters do |t|
      t.string :name
      t.string :race
      t.string :cl
      t.datetime :created_at
    end
  end
end
