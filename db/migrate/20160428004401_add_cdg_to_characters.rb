# Adds integer which tracks the coup de grace speed of a character
class AddCdgToCharacters < ActiveRecord::Migration
  def change
    change_table :characters do |t|
      t.integer :cdg_speed, default: 10
    end
  end
end
