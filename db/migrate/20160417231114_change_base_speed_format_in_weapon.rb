# Change the base_speed row from a string to an integer
class ChangeBaseSpeedFormatInWeapon < ActiveRecord::Migration
  def change
    change_column :weapons, :base_speed, :integer
  end
end
