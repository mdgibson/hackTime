# create the masteries table
class CreateMastery < ActiveRecord::Migration
  def change
    create_table :masteries do |t|
      t.integer :speed_mod
      t.integer :attack_mod
      t.integer :dmg_mod
      t.integer :def_mod
      t.integer :reach_mod
    end
  end
end
