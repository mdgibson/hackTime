require 'sinatra/base'
require 'sinatra/activerecord'
require 'sinatra-websocket'
require 'sinatra/json'
require 'sinatra/reloader'
require_relative 'db/models'
require_relative 'app/controllers/init'
require 'tilt/haml'

# Inherit sinatra base class stuff and setup defaults and routes
class HackTime < Sinatra::Base
  register Sinatra::ActiveRecordExtension
  configure :development do
    register Sinatra::Reloader
  end
  use Rack::MethodOverride #HTTP put requests pass as DELETE with hidden variable
  set :database, adapter: 'sqlite3', database: 'data.sqlite3'
  set :server, 'thin'
  set :sockets, []
  @@time = 0

  def self.time
    @@time
  end

  configure do
    set :app_file, __FILE__
  end

  get '/' do
    haml :index
  end

  post '/query/?' do
    if params[:query_type] == 'Player'
      redirect '/player', 307
    elsif params[:query_type] == 'Weapon'
      name = params[:name].gsub ' ', '%20'
      redirect '/weapon/' + name
    end
  end
end
