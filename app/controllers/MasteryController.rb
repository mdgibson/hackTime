require 'sinatra/base'

class HackTime < Sinatra::Base
  get '/masteries/?' do
    @masteries = Mastery.all
    haml :mastery_index
  end

  # Show mastery page
  get '/mastery/:id/?' do
    @mastery = Mastery.find params[:id]
    haml :mastery_show
  end

  # Make a new Mastery Page
  get '/new_mastery/?:character_name/?' do
    @name = (params[:character_name] ? params[:character_name] : '').to_s.gsub '%20', ' '
    @character = Character.find_by! name: params[:character_name]
    @weapons = Weapon.all
    haml :new_mastery
  end

  # Create the mastery from new mastery page
  post '/mastery/?' do
    mastery = Mastery.where(
      character_id: params[:character_id], weapon_id: params[:weapon_id]
    ).first_or_create
    mastery.update(params)
    if mastery.save
      redirect '/character/' + mastery.character_id.to_s
    else
      'failed commit'
    end
  end
end
