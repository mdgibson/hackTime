require 'sinatra/base'

class HackTime < Sinatra::Base
  get '/weapons/?' do
    @weapons = Weapon.all
    haml :weapon_index
  end

  get '/weapon/:name_or_id/?' do
    begin
      @weapon = Weapon.find_by! name: params[:name_or_id]
    rescue ActiveRecord::RecordNotFound
      begin
        @weapon = Weapon.find params[:name_or_id]
      rescue ActiveRecord::RecordNotFound
        @weapon = Weapon.new
        @weapon.name = params[:name_or_id]
      end
    end
    haml :weapon_show
  end

  post '/weapon/?' do
    weapon = Weapon.where(name: params[:name]).first_or_create
    weapon.update(params)
    if weapon.save
      redirect '/weapons/'
    else
      'failed commit'
    end
  end

  delete '/weapon/:id/?' do
    weapon = Weapon.find params[:id]
    weapon.destroy
    redirect '/weapons/'
  end

  get '/new/weapon/?' do
    haml :new_weapon
  end

  post '/new/weapon/?' do
    begin
      weapon = Weapon.find_by! name: params[:name]
      redirect 'weapon/' + weapon.id.to_s
    rescue ActiveRecord::RecordNotFound
      @weapon = Weapon.create(params)
      redirect '/weapons/'
    end
  end
end
