require 'sinatra/base'

class HackTime < Sinatra::Base
  get '/players/?' do
    @players = Player.all
    haml :player_index
  end

  post '/player/?' do
    begin
      params.delete 'query_type'
      player = Player.find_by! name: params[:name]
    rescue ActiveRecord::RecordNotFound
      player = Player.create(params)
    end
    if player.save
      redirect '/player/' + player.id.to_s
    else
      'failed commit'
    end
  end

  delete '/player/:id/?' do
    player = Player.find params[:id]
    player.destroy
    redirect '/players'
  end

  get '/player/:id/?' do
    @player = Player.find params[:id]
    if !request.websocket?
      haml :player_show
    else
      request.websocket do |ws|
        ws.onopen do
          settings.sockets << ws
          ws.send(JSON.generate({event: 'time_step', data:{time: @@time}}))
        end
        ws.onmessage do |msg|
          msg_hash = JSON.parse(msg)
          case msg_hash['event']
          when 'time_step'
            @@time = msg_hash['data']['time'].to_i
            reply_hash = {time: msg_hash['data']['time']}
            Character.all.each do |c|
              if c.initiative != nil
                initiative = c.next_attack_in(msg_hash['data']['time'].to_i) < 0 ? 0 : c.next_attack_in(msg_hash['data']['time'].to_i)
                c.save
                reply_hash[('attack_' + c.id.to_s).to_sym] = initiative
              end
            end
            reply = JSON.generate({event: 'time_step', data: reply_hash})
            settings.sockets.each do |s|
              s.send(reply)
            end
          when 'end_encounter'
            @@time = 0
            Character.all.each do |c|
              c.initiative = nil
              c.cdg_until = 0
              c.last_time = 0
              c.save
            end
            settings.sockets.each do |s|
              s.send(JSON.generate({event: 'update_time', data:{}}))
            end
          when 'engage'
            character = Character.find msg_hash['data']['character'].to_i
            character.engaged = msg_hash['data']['engaged']
            character.save
            reply = JSON.generate({
              event: 'engage',
              data: {character: msg_hash['data']['character'],
                     engaged: msg_hash['data']['engaged']}
              })
              settings.sockets.each do |s|
                s.send(reply)
              end
          end
        end
        ws.onclose do
          settings.sockets.delete(ws)
        end
      end
    end
  end
end
