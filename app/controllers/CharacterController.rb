require 'sinatra/base'

class HackTime < Sinatra::Base
  get '/characters/?' do
    @characters = Character.all
    haml :character_index
  end

  # Character view page
#  get '/character/:id/?' do
#    @character = Character.find params[:id]
#    haml :character_show
#  end


  get '/character/:id/?' do
    @character = Character.find params[:id]
    if !request.websocket?
      @time = @@time
      haml :character_show
    else
      request.websocket do |ws|
        ws.onopen do
          settings.sockets << ws
        end
        ws.onmessage do |msg|
          msg_hash = JSON.parse(msg)
          case msg_hash['event']
          when 'attack'
            mastery = Mastery.find msg_hash['data']['mastery_id']
            mastery.character.attack(mastery)
            mastery.character.save
            reply = JSON.generate({
              event: 'update_time',
              data: {time: mastery.character.initiative}
              })
            ws.send(reply)
            settings.sockets.each do |s|
              s.send(JSON.generate({event: 'update_stream', data:{
                act: 'add',
                id: mastery.character.id,
                char_name: mastery.character.name,
                initiative: mastery.character.initiative,
                engaged: mastery.character.engaged
                }}))
            end
          when 'cdg'
            @character.cdg(@@time)
            @character.save
            ws.send(JSON.generate({event: 'update_time', data:{time: @character.cdg_speed}}))
          when 'set_cdg_speed'
            @character.cdg_speed = msg_hash['data']['new_speed']
            @character.save
          when 'set_initiative'
            @character.initiative = msg_hash['data']['initiative'].to_i
            @character.last_time = @@time
            @character.save
            ws.send(JSON.generate({event: 'update_time', data:{time:@character.initiative}}))
            action = @character.initiative == nil ? 'remove' : 'add'
            settings.sockets.each do |s|
              s.send(JSON.generate({event: 'update_stream', data:{
                act: action,
                id: @character.id,
                char_name: @character.name,
                initiative: @character.initiative,
                engaged: @character.engaged
                }}))
            end
          when 'set_engaged'
            @character.engaged = msg_hash['data']['engaged']
            @character.save
            reply = JSON.generate({
              event: 'engage_char',
              data: {character: @character.id, engaged: @character.engaged}
              })
            settings.sockets.each do |s|
              s.send(reply)
            end
          else
          end
        end
        ws.onclose do
          settings.sockets.delete(ws)
        end
      end
    end
  end

  # New character creation page
  get '/player/:player_id/character/?' do
    @player = Player.find params[:player_id]
    haml :player_new_char
  end

  # Create character from character creation page
  post '/player/:player_id/character/?' do
    params.delete 'splat'
    params.delete 'captures'
    Character.create(params)

    redirect '/player/' + params[:player_id]
  end

  # Duplicate selected characters
  post '/player/:id/character_dup/?' do
    player = Player.find params[:id]
    copies = params[:num_copies].to_i
    params.delete 'splat'
    params.delete 'captures'
    params.delete '_method'
    params.delete 'id'
    params.delete 'num_copies'

    params.keys.each do |id|
      character_orig = Character.find id
      (0...copies).each do
        character_new = character_orig.dup

        character_orig.masteries.each do |mastery|
          character_new.masteries << mastery.dup
        end

        character_new.masteries.each {|mastery| mastery.save}
        character_new.save
      end

      Character.where(name: character_orig.name).each_with_index do |c, i|
        c.name = i == 0 ? c.name : c.name + '_' + i.to_s
        c.save
      end

    end
    redirect '/player/' + player.id.to_s
  end

  # Delete single character
  delete '/character/:id/?' do
    character = Character.find params[:id]
    player_id = character.player.id
    character.destroy
    redirect "/player/#{player_id}"
  end

  # Delete multiple characters at once
  delete '/player/:id/characters/?' do
    params.delete 'splat'
    params.delete 'captures'
    params.delete '_method'
    player = Player.find params[:id]
    params.delete 'id'

    params.keys.each { |id| Character.find(id).destroy }

    redirect '/player/' + player.id.to_s
  end

  # Assign starting initiative to a character
  post '/character/:character_id/initiative' do
    character = Character.find params[:character_id]
    character.initiative = params[:initiative]
    character.save

    redirect '/character/' + params[:character_id]
  end
end
