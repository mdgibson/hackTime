require 'sinatra/base'

class HackTime < Sinatra::Base
  # Attack with weapon
  post '/attack/:mastery_id/?' do
    mastery = Mastery.find params[:mastery_id]
    swing_time = mastery.weapon.base_speed + mastery.speed_mod
    mastery.character.update_attribute(
      :initiative, mastery.character.initiative += swing_time
    )

    redirect "/character/#{mastery.character.id}"
  end

  # Attack with Coup De Grace weapon
  post '/attack/coupdegrace/:character_id/?' do
    swing_time = params[:speed].to_i
    character = Character.find params[:character_id]
    character.update_attribute(:initiative, character.initiative += swing_time)

    redirect '/character/' + params[:character_id]
  end
end
